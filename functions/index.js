'use strict'

// referrence: https://developers.google.com/actions/assistant/surface-capabilities
// Note : SimulatorでのNewSurfaceは日本語での挙動が良くありません、実機にてテストすること

const { actionssdk, NewSurface, Suggestions } = require('actions-on-google')
const functions = require('firebase-functions')

const app = actionssdk({ debug: true })

app.intent('actions.intent.MAIN', conv => {
  const screenAvailable = conv.available.surfaces.capabilities.has(
    'actions.capability.SCREEN_OUTPUT'
  )

  const context = 'スマートフォンに通知を送るテストです。' // 転送前のask
  const notification = 'テスト用通知' // スマートフォンのnotificationに表示される
  const capabilities = ['actions.capabilitiy.SCREEN_OUTPUT'] // 転送先デバイスの選択
  if (screenAvailable) {
    // conv.ask('こんにちは') // NewSurfaceでは重複askは動作しないのでこれは無効、contextで定義すること
    conv.ask(new NewSurface({ context, notification, capabilities }))
  } else {
    conv.close(
      'あなたのアカウントには画面を持つデバイスが登録されておりません。終了します。'
    )
  }
})

app.intent('actions.intent.NEW_SURFACE', (conv, input) => {
  // https://actions-on-google.github.io/actions-on-google-nodejs/classes/conversation_argument.arguments.html#get
  const status = conv.arguments.get('NEW_SURFACE').status

  console.log('status', status)

  if (status === 'OK') {
    // showPicture(conv, pictureType)

    conv.ask(`通知による表示です。見えますか？`)
    conv.ask(new Suggestions(['はい', 'イエス']))
  } else {
    conv.close(`キャンセルしました。終了します。`)
  }
})

app.intent('actions.intent.TEXT', (conv, input) => {
  conv.close(`ならもう用はない。`)
})

exports.actionsSdkFirebaseFulfillment = functions.https.onRequest(app)
